# 第八单元 项目实战-01

## **一、昨日知识点回顾**

```
promise
Generator函数
async、await
```

------

## **二、本单元知识点概述**

### （Ⅰ）指定教材

​	无

### （Ⅱ）知识点概述

![](media/e5c3e4eb941b03e5c8f779416c9d0ca.png)

### （Ⅲ）教学时长

​	共4课时，180分钟

------

## **三、本单元教学目标**

### （Ⅰ）重点知识目标

```
项目初始化
实现登录功能
```

### （Ⅱ）能力目标

```
知晓电商业务和后台管理系统之间关系
学会创建项目并初始化
登录流程分析和token验证
提交项目代码到git
```

------

## **四、本单元知识详讲**

### 8.1电商业务概述

#### 8.1.1发展历史★★★

Version 1.起步期
1990-1993年，电子数据交换时代，成为中国电子商务的起步期。
Version 2.雏形期　1993-1997年，政府领导组织开展“三金工程”阶段，为电子商务发展期打下坚实基础。
1993年成立国务院副总理为主席的国民经济信息化联席会议及其办公室，相继组织了金关、金卡、金税等"三金工程"，取得了重大进展。
1996年1月成立国务院国家信息化工作领导小组，由副总理任组长，20多个部委参加，统一领导组织我国信息化建设。
1996年，金桥网 [1]  与因特网正式开通。
1997年，信息办组织有关部门起草编制我国信息化规划。
1997年4月在深圳召开全国信息化工作会议，各省市地区相继成立信息化领导小组及其办公室各省开始制订本省包含电子商务在内的信息化建设规划。
1997年，广告主开始使用网络广告。
1997年4月以来,中国商品订货系统(CGOS)开始运行。
Version 3.发展期 　1998-2000年，互联网电子商务发展阶段。
1998年3月，我国第一笔互联网网上交易成功。
1998年10月，国家经贸委与信息产业部联合宣布启动以电子贸易为主要内容的"金贸工程"，它是一项推广网络化应用、开发电子商务在经贸流通领域的大型应用试点工程.。
1999年3月8848等B2C网站正式开通，网上购物进入实际应用阶段．
1999年兴起政府上网、企业上网，电子政务（政府上网工程）、网上纳税、网上教育（湖南大学、浙江大学网上大学），远程诊断（北京、上海的大医院）等广义电子商务开始启动，并已有试点，并进入实际试用阶段。
Version 4.稳定期 　2000-2009年，电子商务逐渐以从传统产业B2B为主体，标志着电子商务已经进入可持续性发展的稳定期。
Version 5.成熟期 　3G的蓬勃发展促使全网全程的电子商务V5时代成型。

#### 8.1.2客户使用的业务服务PC端、小程序、webapp、hybrid★★★

- PC端 是相对于移动端的一个概念，泛指运行在电脑浏览器上的web应用或者这pc端的软件

- webAPP：例如浏览器中的网页、支付宝app首页可点击的“图标”，其大多是基于浏览器运行，但不仅仅限于浏览器中使用

- HybridAPP(混合app)
  其将原生APP和WebAPP进行融合，也是目前比较受欢迎的开发模式，应用大框架是原生的，其它部分内容通过网页进行封装(用户经常操作的部分采取webapp来做，不经常操作更改的部分原生app来做)，例如天猫；优点：其方便更新的同时，也能保证核心功能的交互体验；
- 
  微信小程序：2016年开启内测，2017年上线，其是基于“微信环境”运行的应用；其遵循系列规范制作，其对内存要求“小于等于2M，大于2M不允许上线”；微信小程序适合的应用：用户停留时间少，频次低，例如饿了么、滴滴、单车、墨迹天气等，例如直播、游戏类应用并不适合做成微信小程序(太重了)，还有一类应用其会将常用功能拆分为微信小程序，例如微博；(2/8定律也适用，80%的用户使用的是20%的常用功能，其并不关注其它功能的使用，类似于此种应用，开发“简版”应用->微信小程序就很合适)；


#### 8.1.3业务模式B2C、B2B、C2C、C2B、O2O、B2B2C、F2C★★★

- B2C是指电子商务的一种模式，也是直接面向消费者销售产品和服务商业零售模式。

- B2B（也有写成 BTB，是Business-to-Business的缩写）是指企业与企业之间通过专用网络或Internet，进行数据信息的交换、传递，开展交易活动的商业模式。

- C2C是电子商务的专业用语，意思是个人与个人之间的电子商务，其中C指的是消费者，因为消费者的英文单词是Customer（Consumer），所以简写为C，又因为英文中的2的发音同to，所以C to C简写为C2C，C2C即 Customer（Consumer） to Customer（Consumer）。

- c2b（Customer to Business，即消费者到企业），是互联网经济时代新的商业模式。这一模式改变了原有生产者（企业和机构）和消费者的关系，是一种消费者贡献价值（Create Value）， 企业和机构消费价值（CustomerValue）。

- O2O，是Online To Offline的缩写即在线离线/线上到线下，是指将线下的商务机会与互联网结合，让互联网成为线下交易的平台，这个概念最早来源于美国。

- B2B2C是一种电子商务类型的网络购物商业模式，B是BUSINESS的简称，C是CUSTOMER的简称，第一个B指的是商品或服务的供应商，第二个B指的是从事电子商务的企业，C则是表示消费者。

- F2C指的是Factory to customer，即从厂商到消费者的电子商务模式。有时写作F to C，但为了简便干脆用其谐音F2C(2即to)。


### 8.2功能介绍

#### 8.2.1用户管理★★★★

添加用户

![](media/adduser.jpg)

编辑用户

![](media/edit.jpg)

删除用户

![](media/delUser.jpg)

#### 8.2.2权限管理★★★★

* 权限列表-通过路由展示权限列表组件
* 权限列表-通过路由展示权限列表组件
* 权限列表-调用API获取权限列表的数据
* 权限列表-渲染权限列表UI结构
* 用户-角色-权限 三者之间的关系

![](media/qxgl.jpg)



#### 8.2.3商品管理★★★★

* 商品添加
* 商品修改

![](media/goods.jpg)



![](media/addgoods.jpg)

#### 8.2.4订单管理★★★★

* 创建Order.vue组件及规则
* 发送请求获取订单列表数据
* 将数据绑定到表格中展示
* 实现分页

![](media/order.jpg)



#### 8.2.5登陆、登出★★★★

A.登录状态保持
如果服务器和客户端同源，建议可以使用cookie或者session来保持登录状态
如果客户端和服务器跨域了，建议使用token进行维持登录状态。

B.登录逻辑：
在登录页面输入账号和密码进行登录，将数据发送给服务器
服务器返回登录的结果，登录成功则返回数据中带有token
客户端得到token并进行保存，后续的请求都需要将此token发送给服务器，服务器会验证token以保证用户身份。

![](media/login.jpg)

#### 8.2.6分类管理★★★★

- 新建分支goods_cate
- 创建子级路由
- 添加组件基本布局
- 请求分类数据
- 使用插件展示数据

#### 8.2.7数据统计★★★★

创建数据统计路由组件并添加路由规则

导入ECharts并使用

### 8.3项目开发模式和技术选型

#### 8.3.1前后端分离★★★

在前后端分离的应用模式中，后端仅返回前端所需的数据，不再渲染HTML页面，不再控制前端的效果。至于前端用户看到什么效果，从后端请求的数据如何加载到前端中，都由前端自己决定，网页有网页的处理方式，App有App的处理方式，但无论哪种前端，所需的数据基本相同，后端仅需开发一套逻辑对外提供数据即可。

在前后端分离的应用模式中 ，前端与后端的耦合度相对较低。

在前后端分离的应用模式中，我们通常将后端开发的每个视图都称为一个接口，或者API，前端通过访问接口来对数据进行增删改查。

![](media/1594222351(1).jpg)

#### 8.3.2前端Vue全家桶 + Echarts★★★

vue全家桶  vue-cli + vue + vuex + vue-router + axios + element-ui

数据可视化模块 echarts

#### 8.3.3后端NodeJS、Express、JWT、MySQL★★★

nodejs 服务端的JavaScript环境

express 基于node的web服务端框架

JWT JSON Web Token（JWT）是目前最流行的跨域身份验证解决方案

Mysql 关系型数据库

### 8.4项目初始化

#### 8.4.1脚手架安装★★★

npm install vue-cli -g

#### 8.4.2通过脚手架创建项目★★★

vue init webpack 项目名称

#### 8.4.3路由配置★★★★

```javascript
import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    { path: '/home', component: Home }
  ]
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  //     next()  放行    next('/login')  强制跳转

  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
```



#### 8.4.4引入ElementUI★★★

import { Button, Form, FormItem, Input, Message } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
// 进行全局挂载：
Vue.prototype.$message = Message

#### 8.4.5引入Axios★★★

打开main.js，import axios from 'axios';
设置请求的根路径：axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/';
挂载axios：Vue.prototype.$http = axios;

#### 8.4.6初始化Git仓库★★★

git init

把初始化的代码提交到本地仓库

### 8.5码云相关配置

#### 8.5.1注册码云★★★★

按照提示完成注册 

注册地址 https://gitee.com/signup

![](media/my.jpg)

#### 8.5.2安装Git★★★★

下载适合自己电脑的客户端git https://git-scm.com/downloads

#### 8.5.3通过终端命令创建公钥★★★

在本地创建公钥：在终端运行：ssh-keygen -t rsa -C "xxx@xxx.com"

找到公钥地址

Your identification has been saved in /c/Users/My/.ssh/id_rsa.
Your public key has been saved in /c/Users/My/.ssh/id_rsa.pub.
当我们创建公钥完毕之后，请注意打印出来的信息“Your public key has been saved in”
/c/Users/My/.ssh/id_rsa.pub : c盘下面的Users下面的My下面的.ssh下面的id_rsa.pub就是我们创建好的公钥了

打开id_rsa.pub文件，复制文件中的所有代码，点击码云中的SSH公钥，将生成的公钥复制到公钥中

![image-20210312201108849](C:\Users\dell\AppData\Roaming\Typora\typora-user-images\image-20210312201108849.png)

#### 8.5.4公钥、码云进行关联★★★

打开id_rsa.pub文件，复制文件中的所有代码，点击码云中的SSH公钥，将生成的公钥复制到公钥中

![](media/1594220938(1).jpg)

#### 8.5.5公钥测试★★★

打开终端，输入命令
ssh -T git@gitee.com

#### 8.5.6新建仓库★★★

点击码云右上角的+号->新建仓库

![](media/1594221016(1).jpg)

#### 8.5.7Git基本信息配置★★★

![](media/1594221075(1).jpg)

#### 8.5.8和远程仓库进行关联★★★

![](media/1594221134(1).jpg)

### 8.6postman接口测试

#### 8.6.1无参数API测试★★★★

![](media/1594221203(1).jpg)

#### 8.6.2有参数API测试★★★★

![](media/1594221203(1).jpg)

#### 8.6.3带有token API测试★★★★

![](media/1594221361(1).jpg)

### 8.7 实现登录功能

#### 8.7.1	登录逻辑和初始化项目★★★★★

A. 登录状态保持
如果服务器和客户端同源，建议可以使用cookie或者session来保持登录状态
如果客户端和服务器跨域了，建议使用token进行维持登录状态。

B. 登录逻辑：
在登录页面输入账号和密码进行登录，将数据发送给服务器
服务器返回登录的结果，登录成功则返回数据中带有token
客户端得到token并进行保存，后续的请求都需要将此token发送给服务器，服务器会验证token以保证用户身份。

C. 添加新分支login，在login分支中开发当前项目vue_shop：
打开vue_shop终端，使用git status确定当前项目状态。
确定当前工作目录是干净的之后，创建一个分支进行开发，开发完毕之后将其合并到master
git checkout -b login
然后查看新创建的分支：git branch
确定我们正在使用login分支进行开发

然后执行vue ui命令打开ui界面，然后运行serve，运行app查看当前项目效果

发现现在是一个默认页面，我们需要进行更改，打开项目的src目录，点击main.js文件（入口文件）

```
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

```

再打开App.vue(根组件)，将根组件的内容进行操作梳理(template中留下根节点，script中留下默认导出，去掉组件，style中去掉所有样式)

```
<template>
  <div id="app">
    <router-view></router-view>
  </div>
</template>

<script>
export default {
  name: 'app'
}
</script>

<style>
</style>
```

再打开router.js(路由)，将routes数组中的路由规则清除，然后将views删除，将components中的helloworld.vue删除

```
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    
  ]
})
```

#### 8.7.2	新建Login.vue组件★★★★★

在components文件夹中新建Login.vue组件,添加template，script，style标签,style标签中的scoped可以防止组件之间的样式冲突，没有scoped则样式是全局的

```
<template>
    <div class="login_container">
        
    </div>
</template>

<script>
export default {
  
}
</script>

<style lang="less" scoped>
.login_container {
  background-color: #2b5b6b;
  height: 100%;
}

</style>
```

在router.js中导入组件并设置规则
在App.vue中添加路由占位符

```
const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login }
  ]
})
```

当我们给Login.vue中的内容添加样式的时候，会报错“缺少less-loader”，需要配置less加载器（开发依赖），安装less(开发依赖)

然后需要添加公共样式，在assets文件夹下面添加css文件夹，创建global.css文件,添加全局样式

```
/* 全局样式表 */
html,body,#app{
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0; 
}
```

在main.js中导入global.css，使得全局样式生效
import "./assets/css/global.css"
然后Login.vue中的根元素也需要设置撑满全屏（height:100%）
最终Login.vue文件中的代码如下

```
<template>
    <div class="login_container">
        <!-- 登录盒子  -->
        <div class="login_box">
            <!-- 头像 -->
            <div class="avatar_box">
                <img src="../assets/logo.png" alt="">
            </div>
            <!-- 登录表单 -->
            <el-form :model="loginForm" ref="LoginFormRef" :rules="loginFormRules" label-width="0px" class="login_form">
                <!-- 用户名 -->
                <el-form-item prop="username">
                    <el-input v-model="loginForm.username" prefix-icon="iconfont icon-user" ></el-input>
                </el-form-item> 
                <!-- 密码 -->
                <el-form-item prop="password">
                    <el-input type="password" v-model="loginForm.password" prefix-icon="iconfont icon-3702mima"></el-input>
                </el-form-item> 
                <!-- 按钮 -->
                <el-form-item class="btns">
                    <el-button type="primary" @click="login">登录</el-button>
                    <el-button type="info" @click="resetLoginForm">重置</el-button>
                </el-form-item> 
            </el-form>
        </div>
    </div>
</template>

<script>
export default {
  data() {
    return {
      //数据绑定
      loginForm: {
        username: 'admin',
        password: '123456'
      },
      //表单验证规则
      loginFormRules: {
        username: [
          { required: true, message: '请输入登录名', trigger: 'blur' },
          {
            min: 3,
            max: 10,
            message: '登录名长度在 3 到 10 个字符',
            trigger: 'blur'
          }
        ],
        password: [
          { required: true, message: '请输入密码', trigger: 'blur' },
          {
            min: 6,
            max: 15,
            message: '密码长度在 6 到 15 个字符',
            trigger: 'blur'
          }
        ]
      }
    }
  },
  //添加行为，
  methods: {
    //添加表单重置方法
    resetLoginForm() {
      //this=>当前组件对象，其中的属性$refs包含了设置的表单ref
      //   console.log(this)
      this.$refs.LoginFormRef.resetFields()
    },
    login() {
      //点击登录的时候先调用validate方法验证表单内容是否有误
      this.$refs.LoginFormRef.validate(async valid => {
        console.log(this.loginFormRules)
        //如果valid参数为true则验证通过
        if (!valid) {
          return
        }

        //发送请求进行登录
        const { data: res } = await this.$http.post('login', this.loginForm)
        //   console.log(res);
        if (res.meta.status !== 200) {
          return this.$message.error('登录失败:' + res.meta.msg) //console.log("登录失败:"+res.meta.msg)
        }

        this.$message.success('登录成功')
        console.log(res)
        //保存token
        window.sessionStorage.setItem('token', res.data.token)
        // 导航至/home
        this.$router.push('/home')
      })
    }
  }
}
</script>

<style lang="sass" scoped>
.login_container {
  background-color: #2b5b6b;
  height: 100%;
}
.login_box {
  width: 450px;
  height: 300px;
  background: #fff;
  border-radius: 3px;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  .avatar_box {
    height: 130px;
    width: 130px;
    border: 1px solid #eee;
    border-radius: 50%;
    padding: 10px;
    box-shadow: 0 0 10px #ddd;
    position: absolute;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fff;
    img {
      width: 100%;
      height: 100%;
      border-radius: 50%;
      background-color: #eee;
    }
  }
}
.login_form {
  position: absolute;
  bottom: 0;
  width: 100%;
  padding: 0 20px;
  box-sizing: border-box;
}
.btns {
  display: flex;
  justify-content: flex-end;
}
</style>

```

其中我们有用到一下内容，需要进行进一步处理：

#### 8.7.3	添加element-ui的表单组件★★★★★

在plugins文件夹中打开element.js文件，进行elementui的按需导入

```
import Vue from 'vue'
import { Button } from 'element-ui'
import { Form, FormItem } from 'element-ui'
import { Input } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
```

#### 8.7.4	添加第三方字体★★
复制素材中的fonts文件夹到assets中,在入口文件main.js中导入

```
import './assets/fonts/iconfont.css'
```

然后直接在
```
<el-input prefix-icon="iconfont icon-3702mima"></el-input>
```
接着添加登录盒子

#### 8.7.5	添加表单验证的步骤★★★★

1).给<el-form>添加属性:rules="rules"，rules是一堆验证规则，定义在script中
2).在script中添加

```
rules：export default{ 
data(){return{......, 
rules: {
          name: [
            { required: true, message: '请输入活动名称', trigger: 'blur' },
            { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' }
          ],
          region: [
            { required: true, message: '请选择活动区域', trigger: 'change' }
          ]
}......
```

3).通过<el-form-item>的prop属性设置验证规则<el-form-item label="活动名称" prop="name">

4.导入axios以发送ajax请求
打开main.js，import axios from 'axios';
设置请求的根路径：axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/';
挂载axios：Vue.prototype.$http = axios;

#### 8.7.6	配置弹窗提示★★★★★

在plugins文件夹中打开element.js文件，进行elementui的按需导入
```
import {Message} from 'element-ui'
```
进行全局挂载：
```
Vue.prototype.$message = Message;
```
在login.vue组件中编写弹窗代码：this.$message.error('登录失败')

### 8.8	登录成功之后的操作

#### 8.8.1	将后台返回的token保存到sessionStorage中★★★★★

登录成功之后，需要将后台返回的token保存到sessionStorage中
操作完毕之后，需要跳转到/home

```
login() {
      //点击登录的时候先调用validate方法验证表单内容是否有误
      this.$refs.LoginFormRef.validate(async valid => {
        console.log(this.loginFormRules)
        //如果valid参数为true则验证通过
        if (!valid) {
          return
        }

        //发送请求进行登录
        const { data: res } = await this.$http.post('login', this.loginForm)
        //   console.log(res);
        if (res.meta.status !== 200) {
          return this.$message.error('登录失败:' + res.meta.msg) //console.log("登录失败:"+res.meta.msg)
        }

        this.$message.success('登录成功')
        console.log(res)
        //保存token
        window.sessionStorage.setItem('token', res.data.token)
        // 导航至/home
        this.$router.push('/home')
      })
    }
```

#### 8.8.2	组件Home.vue，并为之添加路由规则★★★★★

添加一个组件Home.vue，并为之添加规则

```
<template>
    <div>
        this is home
        <el-button type="info" @click="logout"> 退出 </el-button>
    </div>
</template>

<script>
export default {
  methods: {
    logout() {
      window.sessionStorage.clear()
      this.$router.push('/login')
    }
  }
}
</script>

<style lang='less' scoped>
</style>
```
添加路由规则
```
const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    { path: '/home', component: Home }
  ]
})
```


#### 8.8.3	添加路由守卫★★★★★

如果用户没有登录，不能访问/home,如果用户通过url地址直接访问，则强制跳转到登录页面
打开router.js

```
import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    { path:'/', redirect:'/login'},
    { path:'/login' , component:Login },
    { path:'/home' , component:Home}
  ]
})

//挂载路由导航守卫,to表示将要访问的路径，from表示从哪里来，next是下一个要做的操作
router.beforeEach((to,from,next)=>{ 
  if(to.path === '/login')
    return next();
  
  //获取token
  const tokenStr = window.sessionStorage.getItem('token');

  if(!tokenStr)
    return next('/login');

  next();

})

export default router 
```


#### 8.8.4	实现退出功能★★★

在Home组件中添加一个退出功能按钮,给退出按钮添加点击事件，添加事件处理代码如下：

```
export default {
    methods:{
        logout(){
            window.sessionStorage.clear();
            this.$router.push('/login');
        }
    }
}
```

#### 8.9	补充

#### 8.9.1	处理ESLint警告★★★

打开脚手架面板，查看警告信息

默认情况下，ESLint和vscode格式化工具有冲突，需要添加配置文件解决冲突。
在项目根目录添加 .prettierrc文件

```
{
    "semi":false,
    "singleQuote":true
}
```

打开.eslintrc.js文件，禁用对 space-before-function-paren的检查:

```
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'space-before-function-paren' : 0
  },
```

#### 8.9.2	合并按需导入的element-ui★★★★

```
import Vue from 'vue'
import { Button, Form, FormItem, Input, Message } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
// 进行全局挂载：
Vue.prototype.$message = Message
```

#### 8.9.3	将代码提交到码云★★★
新建一个项目终端,输入命令git status查看修改过的与新增的文件内容
将所有文件添加到暂存区：git add .
将所有代码提交到本地仓库：git commit -m "添加登录功能以及/home的基本结构"
查看分支：git branch 发现所有代码都被提交到了login分支
将login分支代码合并到master主分支，先切换到master：git checkout master
在master分支进行代码合并：git merge login
将本地的master推送到远端的码云：git push

推送本地的子分支到码云，先切换到子分支：git checkout 分支名
然后推送到码云：git push -u origin 远端分支名


------

## **五、本单元知识总结**

```
1.码云
2.postman
3.登陆过程
4.token
```

------

## **六、作业安排**

### （Ⅰ）课后作业

	课堂代码

### （Ⅱ）预习作业

```
1.预习第九单元，项目实战-02
```

###### 预习附录：

```

```

